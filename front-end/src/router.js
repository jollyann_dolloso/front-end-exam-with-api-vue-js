import { createRouter, createWebHistory } from 'vue-router';

import Top from './pages/top/Top.vue';
import NotFound from './pages/NotFound.vue';
import NewsDetail from './pages/news/NewsDetail.vue'
import NewsForm from './pages/news/NewsForm.vue'
import store from './store/index.js';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/top' },
    { path: '/top', component: Top },
    { path: '/news/:id', component: NewsDetail, props: true},
    { path: '/news/edit/:id', component: NewsForm, props: true, meta: {requiresAuth: true}},
    { path: '/news/add', component: NewsForm, meta: {requiresAuth: true}},
    { path: '/:notFound(.*)', component: NotFound }
  ]
});

router.beforeEach(function(to, _, next) {
  if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
    next('/top');
  } else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
    next('/top');
  } else {
    next();
  }
});

export default router;
