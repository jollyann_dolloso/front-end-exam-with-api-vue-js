import moment from 'moment';

export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  }
}

export const checkValidity = (controlName, value, rules) => {
  let isValid = true;
  let errorMessage = [];

  if (!rules) { return true; }

  if (value.trim() !== '' && isValid && rules.isEmail) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isValidEmail = re.test(value);
      if (!isValidEmail) {
        isValid = isValidEmail;
        errorMessage.push(`You have entered invalid ${controlName} address. Please try again.`)
      }
  }

  if (rules.required) {
    isValid = value.trim() !== '' && isValid;
    if (!isValid) {
      errorMessage.push(`*${controlName} is required.`)
    }
  }

  if (rules.minLength) {
    isValid = value.length >= rules.minLength && isValid;
    if (!isValid) {
      errorMessage.push(`${controlName} does not reach the minimum ${rules.minLength} length.`)
    }
  }

  if (rules.maxLength) {
    isValid = value.length <= rules.maxLength && isValid;
    if (!isValid) {
      errorMessage.push(`${controlName} does not reach the maximum ${rules.minLength} length.`)
    }
  }
  return {isValid: isValid, errorMessage: errorMessage};
}

export const formatDate = (date) => {
  return moment(date).format('YYYY.MM.DD')
}

export const formatDateFromNow = (date) => {
  return moment(date).fromNow();
}