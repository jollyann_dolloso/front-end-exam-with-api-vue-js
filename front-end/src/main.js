import { createApp, provide, h } from 'vue';

import router from './router.js';
import store from './store/index.js';

import { ApolloClient, InMemoryCache , createHttpLink} from '@apollo/client/core';
import { DefaultApolloClient } from '@vue/apollo-composable'
import { setContext } from '@apollo/client/link/context';

import App from './App.vue';

// Base component imports
import BaseSlider from './components/ui/BaseSlider.vue';
import BaseCard from './components/ui/BaseCard.vue';
import BaseButton from './components/ui/BaseButton.vue';
import BaseBreadcrumbs from './components/ui/BaseBreadcrumbs.vue';
import BaseDialog from './components/ui/BaseDialog.vue';
import BaseSpinner from './components/ui/BaseSpinner.vue';
import BaseMessage from './components/ui/BaseMessage.vue';

const httpLink = createHttpLink({
  uri: 'http://localhost:4000',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? token : "",
    }
  }
});

const defaultClient = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    defaultOptions: {
      $query: {
        fetchPolicy: 'cache-and-network'
      },
    },
});

const app = createApp(
  {
    setup() {
      provide(DefaultApolloClient, defaultClient)
    },
    render() {
      return h(App)
    }
});

app.use(router);
app.use(store);
app.component('base-slider', BaseSlider);
app.component('base-card', BaseCard);
app.component('base-button', BaseButton);
app.component('base-breadcrumbs', BaseBreadcrumbs);
app.component('base-dialog', BaseDialog);
app.component('base-spinner', BaseSpinner);
app.component('base-message', BaseMessage);

app.mount('#app');
