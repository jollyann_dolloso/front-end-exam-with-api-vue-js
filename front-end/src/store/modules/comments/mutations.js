export default {
  getComments(state, newsId) {
    let comments = state.commentsData.filter(comment => comment.newsId == newsId)
    state.comments = {...comments};
  },
  addComment(state, payload) {
    state.commentsData.unshift(payload);
  }
}
