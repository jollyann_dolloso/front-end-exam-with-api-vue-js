export default {
  loadComments(context, payload) {
    // write http request here in the future
    context.commit('getComments', payload.newsId);
  },
  addComment(context, payload) {
    const {newsId, comment} = payload;
    let newComment = {
      id: new Date(),
      newsId: newsId,
      comment: comment,
      postedDate: new Date(),
    }
    context.commit('addComment', newComment)
  }
}
