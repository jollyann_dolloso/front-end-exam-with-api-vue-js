let timer
export default {
  toggleAuthForm(context) {
    const { isToggleAuthForm } = context.getters;
    context.commit('toggleAuthForm', !isToggleAuthForm);
  },

  login(context, payload) {
    return context.dispatch('auth', {
      ...payload,
      mode: 'login',
    });
  },

  register(context, payload) {
    return context.commit('toggleAuthForm', true)
  },

  auth(context, payload) {
    const expiresIn = 5000;
    const expirationDate = new Date().getTime() + expiresIn;
    // timer = setTimeout(function() {
    //   context.dispatch('autoLogout');
    // }, expirationDate);

    context.commit('setUser', {
      token: payload.token,
    });
  },

  tryLogin(context) {
    const token = localStorage.getItem('token');
    const tokenExpiration = localStorage.getItem('tokenExpiration');
    
    const expiresIn = tokenExpiration;

    if (expiresIn < 0) {
      return;
    }

    // timer = setTimeout(function() {
    //   context.dispatch('autoLogout');
    // }, expiresIn);

    if (token) {
      context.commit('setUser', {
        token: token,
        // userId: userId,
      });
    }
  },

  logout(context) {
    localStorage.removeItem('token');
    localStorage.removeItem('tokenExpiration');
    clearTimeout(timer);

    context.commit('setUser', {
      token: null,
    });
    context.dispatch('toggleAuthForm');
  },

  autoLogout(context) {
    context.dispatch('logout');
    context.commit('setAutoLogout');
    context.dispatch('toggleAuthForm');
  }
};
