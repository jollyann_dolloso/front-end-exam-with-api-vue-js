export default {
  toggleAuthForm(state, payload) {
    state.isToggleAuthForm = payload;
  },
  setUser(state, payload) {
    state.token = payload.token;
    state.didAutoLogout = false;
  },
  setAutoLogout(state) {
    state.didAutoLogout = true;
  }
}
