import {updateObject} from '../../../common/utility.js';

export default {
  register(state, payload) {
    state.news.unshift(payload);
  },
  setNews(state, payload) {
    state.news = payload;
  },
  update(state, payload) {
    const updateNews = state.news.find(update => update.id === +payload.id)
    Object.assign(updateNews, payload);
  }
};
