import moment from 'moment';

export default {
   loadNews(context) {
    // write http request here in the future
    const newsArticles = [];
    const { news } = context.getters; // temporary for static data
    for (const key in news) {
      const newsItem = {
        id: news[key].id,
        thumbnail: news[key].thumbnail,
        publishDate: moment(news[key].publishDate).format('YYYY.MM.DD'),
        title: news[key].title,
        content: news[key].content,
        authorId: news[key].authorId,
      };
      newsArticles.push(newsItem);
    }

    context.commit('setNews', newsArticles);
  },
  addNews(context, payload) {
    const {news} = context.getters;
    let date = new Date();
    const newPost = {
      id: news.length + 1,
      publishDate: date.toString(),
      title: payload.title,
      thumbnail: payload.thumbnail,
      content: payload.content,
    }
    context.commit('register', newPost);
  },
  updateNews(context, payload) {
    let date = new Date();
    const updateNews = {
      id: +payload.id,
      publishDate: date.toString(),
      title: payload.title,
      thumbnail: payload.thumbnail,
      content: payload.content,
    }
    context.commit('update', updateNews);
  }
};
