import { createStore } from 'vuex';

import newsModule from './modules/news/index.js';
import authModule from './modules/auth/index.js';
import commentModule from './modules/comments/index.js';

const store = createStore({
  modules: {
    news: newsModule,
    auth: authModule,
    comments: commentModule
  }
});

export default store;
